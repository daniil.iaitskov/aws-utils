#!/bin/bash

find -L /tmp -type l -maxdepth 1 -exec bash -c '[ -d $(readlink {}) ] && echo KEEP {} || { echo "ReMOVE {}"; rm {}; }' \;
