#!/bin/bash

# remove files older than

S3PATH=
DRY=
AGE_DAYS=14
ROWS_TO_KEEP=10

err() {
    echo "$@" 1>&2
    exit 1;
}

while [ $# -ne 0 ] ; do
    case "$1" in
        -h|--help)
            cat <<EOF
Usage: gc-s3-folder --s3-folder-path s3://my-bucket/a/b/c [ options ]
       removes files only from s3 folder which are older than
       specified and they are direct children of the folder.

       -h --help                print this help
       -p --s3-folder-path <P>  path to the folder for GC
       -d --dry                 just print file name which are going to be removed
       -s --skip <N=10>         how many rows to keep nonetheless their age
       -a --age-days <N>        change age for removal. default is 14
EOF
            exit 1;;
        -p|--s3-folder-path)
            shift; S3PATH="$1" ;;
        -a|--age-days) shift; AGE_DAYS="${1%/}" ;;
        -s|--skip) shift; ROWS_TO_KEEP="$1";;
        -d|--dry) DRY=1 ;;
        *) err "Bad option $1"; exit 1 ;;
    esac
    shift
done

[[ "$ROWS_TO_KEEP" =~ ^[0-9]*$ ]] || err "bad rows to keep: $ROWS_TO_KEEP"
[[ "$AGE_DAYS" =~ ^[1-9][0-9]*$ ]] || err "bad age: $AGE_DAYS"
[ -z "$S3PATH" ] && err "s3 path is not set"
[[ "$S3PATH" =~ ^s3:// ]] || err "bad s3 path: $S3PATH"

export PATH=$PATH:/usr/local/bin

# aws format 2021-08-12 09:48:35
RM_OLDER_THAN="$(date -v-${AGE_DAYS}d +%Y-%m-%d)"

echo "===$(date)=================================================="

aws s3 ls $S3PATH | sort -r | sed "1,$ROWS_TO_KEEP"d | while read DT TS SZ NAME REST ; do
    if [[ "$DT" =~ ^[0-9]{4}[-][0-9]{2}[-][0-9]{2}$ ]] ; then
        if [[ "$DT" < "$RM_OLDER_THAN" ]] ; then
            echo "Remove $S3PATH$NAME (created at $DT $TS; size is $SZ)"
            [ -z "$DRY" ] && aws s3 rm $S3PATH$NAME
        fi
    fi
done
