#!/bin/bash

PERIOD_SECONDS="$[24*3600]"
ARGS=
LABEL=

err() {
    echo "Error: $@" 1>&2
    exit 1
}

set -e # exit on error

while [ $# -ne 0 ] ; do
    case $1 in
        -h) cat<<EOF
Usage: schedule-plist-script [ options ] -s path-to-script
Options:
       -p <SECONDS>      period
       -a <script arg>   script arugment. can be used multiple times.
EOF
            exit 1   ;;
        -s) shift; PATH_TO_SCRIPT="$1";;
        -a) shift; ARGS="$ARGS $1";;
        -p) shift; PERIOD_SECONDS="$1";;
        -l) shift; LABEL="$1";;
        *) err "Bad option $1";;
    esac
            shift
done

[ -z "$PATH_TO_SCRIPT" ] && err "Path to script is not set"
[ -f "$PATH_TO_SCRIPT" ] || {
    TYPED_PATH="$(type "$PATH_TO_SCRIPT")"
    TYPED_PATH="${TYPED_PATH##*is }"
    if [ -f "$TYPED_PATH" ] ; then
        PATH_TO_SCRIPT="$TYPED_PATH"
    else
        err "script $PATH_TO_SCRIPT does not exist"
    fi
}
[ -x "$PATH_TO_SCRIPT" ] || err "script $PATH_TO_SCRIPT is not executable"
[[ "$PERIOD_SECONDS" =~ ^[0-9]+$ ]] || err "period is not number but [$PERIOD_SECONDS]"
[ "$USER" == root ] && err "Don't run as root"

SERVICE_NAME="$(basename "$PATH_TO_SCRIPT")$LABEL-each-$PERIOD_SECONDS-seconds"

SCRIPT_NAME=$SERVICE_NAME
mkdir -p /Users/Shared/$SCRIPT_NAME

while [ -f /Users/Shared/$SCRIPT_NAME/$(basename "$PATH_TO_SCRIPT") ] ; do
    SCRIPT_NAME=${SCRIPT_NAME}1
done

COPIED_SCRIPT=/Users/Shared/$SCRIPT_NAME/$(basename "$PATH_TO_SCRIPT")

if launchctl list | grep -q "$SERVICE_NAME"; then
    err "Service name [$SERVICE_NAME] is already used. Use -l label to disambiguate."
fi

cp $PATH_TO_SCRIPT $COPIED_SCRIPT


sudo chmod 555 $COPIED_SCRIPT
sudo chown root $COPIED_SCRIPT


PLIST_FILE=$HOME/Library/LaunchDaemons/$SERVICE_NAME.plist
cat<<EOF > $PLIST_FILE
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.$SERVICE_NAME</string>
    <key>OnDemand</key>
    <true/>
    <key>ProgramArguments</key>
    <array>
        <string>sudo</string>
        <string>-u</string>
        <string>$USER</string>
        <string>$COPIED_SCRIPT</string>
EOF

for A in ${ARGS[@]} ; do
cat<<EOF >> $PLIST_FILE
        <string>$A</string>
EOF
done

cat<<EOF >> $PLIST_FILE
    </array>
    <key>StartInterval</key>
    <integer>$PERIOD_SECONDS</integer>
    <key>StandardOutPath</key>
    <string>/var/log/$SERVICE_NAME.out.log</string>
    <key>StandardErrorPath</key>
    <string>/var/log/$SERVICE_NAME.err.log</string>
</dict>
</plist>
EOF

sudo chown root $PLIST_FILE || err "chown plist"
sudo chmod 600 $PLIST_FILE || err "chmod plist"
sudo launchctl load $PLIST_FILE || err "launch service $SERVICE_NAME"
